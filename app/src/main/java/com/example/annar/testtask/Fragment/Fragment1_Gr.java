package com.example.annar.testtask.Fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.annar.testtask.DataBase.PhotoContract;
import com.example.annar.testtask.DataBase.PhotoDbHelper;
import com.example.annar.testtask.Photo.ItemPhoto;
import com.example.annar.testtask.R;
import com.example.annar.testtask.Adapter.RecyclerViewAdapter;

import java.util.ArrayList;

/**
 * Created by annar on 24.09.2018.
 */

public class Fragment1_Gr  extends Fragment {
    ArrayList<ItemPhoto> photos = new ArrayList<>();
    private PhotoDbHelper mDbHelper;
    private RecyclerView rv;
    public Fragment1_Gr() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDbHelper =  new PhotoDbHelper(getActivity().getApplicationContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment1_grid, container, false);

        rv = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        rv.setHasFixedSize(true);

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(photos, getActivity().getApplicationContext(), getActivity()
        );
        rv.setAdapter(adapter);

        displayDatabaseInfo();
        GridLayoutManager llm = new GridLayoutManager(getActivity(),3);
        rv.setLayoutManager(llm);

        return rootView;
    }


    public void displayDatabaseInfo() {
        // Создадим и откроем для чтения базу данных
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Зададим условие для выборки - список столбцов
        String[] projection = {
               PhotoContract.PhotoEntry._ID,
                PhotoContract.PhotoEntry.LINK_Photo
                 };

        // Делаем запрос
        Cursor cursor = db.query(
                PhotoContract.PhotoEntry.TABLE_NAME,   // таблица
                projection,            // столбцы
                null,                  // столбцы для условия WHERE
                null,                  // значения для условия WHERE
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);                   // порядок сортировки



        try {
            if(cursor.moveToFirst()) {
                photos.clear();
                photos.add(new ItemPhoto(cursor.getString(0), cursor.getString(1)));

                rv.getAdapter().notifyDataSetChanged();
                while (cursor.moveToNext()) {
                    photos.add(new ItemPhoto(cursor.getString(0), cursor.getString(1)));
                    rv.getAdapter().notifyDataSetChanged();
                }
            }
            else{
                photos.clear();
                rv.getAdapter().notifyDataSetChanged();
            }


        } finally {
            // Всегда закрываем курсор после чтения
            cursor.close();
        }
    }



}
