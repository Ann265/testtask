package com.example.annar.testtask.Photo;

import android.widget.ImageView;

/**
 * Created by annar on 24.09.2018.
 */

public class ItemPhoto {

    private String id;
    private String photo;

    public ItemPhoto(String id, String photo) {
        this.id = id;
        this.photo = photo;
    }


    public void setId(String name) {
        this.id = name;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public String getPhoto() {
        return photo;
    }
}
