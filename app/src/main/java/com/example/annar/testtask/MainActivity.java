package com.example.annar.testtask;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.LightingColorFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import android.widget.LinearLayout;
import android.widget.RadioGroup;

import android.widget.TextView;
import android.widget.Toast;


import com.example.annar.testtask.Adapter.RecyclerViewAdapter;
import com.example.annar.testtask.DataBase.PhotoContract;
import com.example.annar.testtask.DataBase.PhotoDbHelper;
import com.example.annar.testtask.Fragment.Fragment1_Gr;
import com.example.annar.testtask.Fragment.Fragment2_List;
import com.example.annar.testtask.Preference.Preferences;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Preferences preferences;
    private AlertDialog.Builder builder;
    private RadioGroup radioGroup, radioGroup1;
    private static final int ACTION_REQUEST_GALLERY = 0;
    private static final int ACTION_REQUEST_CAMERA = 1;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Fragment1_Gr fr1;
    private Fragment2_List fr2;
    private Uri initialURI;
    private PhotoDbHelper mDbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = new Preferences(getApplicationContext()); //настройки!! Не забыть обработать если пусто
        mDbHelper = new PhotoDbHelper(this); //для работы с базой, чтобы не удалялись фото


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView textViewAnswer = (TextView) findViewById(R.id.action_bar_title);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        Button addphoto = (Button) findViewById(R.id.addPict);
        final Button delete = (Button) findViewById(R.id.deletePict);

        //Установка тэбов, для отображения различных списков
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_launcher_grid);
        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_launcher_list);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletePhoto();
            }
        });
        //Выбор откуда считывание фото и сохранение путей
        addphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Choose Image Source");
                builder.setItems(new CharSequence[] {"Gallery", "Camera"},
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:

                                        // GET IMAGE FROM THE GALLERY
                                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                                        intent.setType("image/*");

                                        Intent chooser = Intent.createChooser(intent, "Choose a Picture");
                                        startActivityForResult(chooser, ACTION_REQUEST_GALLERY);

                                        break;

                                    case 1:
                                        Intent getCameraImage = new Intent("android.media.action.IMAGE_CAPTURE");

                                        File cameraFolder;

                                        if (android.os.Environment.getExternalStorageState().equals
                                                (android.os.Environment.MEDIA_MOUNTED))
                                            cameraFolder = new File(android.os.Environment.getExternalStorageDirectory(),
                                                    "wallpaperPict/");
                                        else
                                            cameraFolder= MainActivity.this.getCacheDir();
                                        if(!cameraFolder.exists())
                                            cameraFolder.mkdirs();

                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
                                        String timeStamp = dateFormat.format(new Date());
                                        String imageFileName = "picture_" + timeStamp + ".jpg";

                                        File photo = new File(Environment.getExternalStorageDirectory(),
                                                "wallpaperPict/" + imageFileName);
                                        getCameraImage.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                        initialURI = Uri.fromFile(photo);

                                        startActivityForResult(getCameraImage, ACTION_REQUEST_CAMERA);

                                        break;

                                    default:
                                        break;
                                }
                            }
                        });

                builder.show();
            }
        });
       // displayofDB();



    }

    void deletePhoto (){
        ArrayList<String> list = RecyclerViewAdapter.getListDelete();
        if (list != null) {
            SQLiteDatabase db = mDbHelper.getReadableDatabase();

            for (int i = 0; i < list.size(); i++) {
                db.execSQL("DELETE FROM " + PhotoContract.PhotoEntry.TABLE_NAME + " WHERE _id=" + list.get(i));
            }
            RecyclerViewAdapter.clearList();
            displayofDB();
        }

    }

    //создание меню справа сверху
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.aboutPhone:
                //информация о телефоне
                DisplayMetrics dm = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(dm);
                int width=dm.widthPixels;
                int height=dm.heightPixels;
                double wi=(double)width/(double)dm.xdpi;
                double hi=(double)height/(double)dm.ydpi;
                double x = Math.pow(wi,2);
                double y = Math.pow(hi,2);
                double screenInches = Math.sqrt(x+y);
                builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Информация ")
                        .setMessage( "Модель: " + Build.MODEL +"\n"
                                + "Марка:" + Build.BRAND + "\n"
                                + "Разрешение" + height + " х " + width + "\n"+
                                "Диагональ"  + screenInches
                                 )
                        .setCancelable(false)
                        .setNegativeButton("Ок",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            case R.id.pref:
                //настройки...запоминание на сколько часов и в каком порядке
//            builder = new AlertDialog.Builder(MainActivity.this);
//                builder.setView(R.layout.dialog_pref);
//                radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
//
//                builder.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
                        //установка настроек

//                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//
//                            @Override
//                            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                                switch (checkedId) {
//                                    case -1:
//                                        Toast.makeText(getApplicationContext(), "Ничего не выбрано1",
//                                                Toast.LENGTH_SHORT).show();
//                                    case R.id.radio1: //хранение в минутах
//                                        preferences.saveData("minute", String.valueOf(8*60));
//                                        break;
//                                    case R.id.radio2:
//                                        preferences.saveData("minute", String.valueOf(4*60));
//                                        break;
//                                    case R.id.radio3:
//                                        preferences.saveData("minute", String.valueOf(60));
//                                        break;
//                                    case R.id.radio4:
//                                        preferences.saveData("minute", String.valueOf(30));
//                                        break;
//                                    default:
//                                        break;
//                                }
//                            }
//                        });
//                        RadioGroup radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
//                        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//
//                            @Override
//                            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                                switch (checkedId) {
//                                    case -1:
//                                        Toast.makeText(getApplicationContext(), "Ничего не выбрано2",
//                                                Toast.LENGTH_SHORT).show();
//                                    case R.id.radio_rand: //хранение в минутах
//                                        preferences.saveData("order", "random");
//                                        break;
//                                    case R.id.radio_next:
//                                        preferences.saveData("order", "next");
//                                        break;
//
//                                    default:
//                                        break;
//                                }
//                            }
//                        });
//                    }
//                });
//
//                builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int arg1) {
//                        dialog.cancel();
//                    }
//                });
//                AlertDialog dialog = builder.create();
//                dialog.show();
//                Button but1 = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
//
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT
//                );
//                params.setMargins(0, 0, 150, 0);
//                but1.setLayoutParams(params);
//
//                Button but2 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
//                return true;

                final CharSequence[] items = {"8 часа","4 часа","1 час","30 минут"};
                AlertDialog dialog;
                // Creating and Building the Dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle("Выберите время");
                builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {


                        switch(item)
                        {
                            case 0:
                                preferences.saveData("minute", String.valueOf(8*60));
                                break;
                            case 1:
                                preferences.saveData("minute", String.valueOf(4*60));

                                break;
                            case 2:
                                preferences.saveData("minute", String.valueOf(60));
                                break;
                            case 3:
                                preferences.saveData("minute", String.valueOf(30));
                                break;

                        }
                        dialog.dismiss();
                    }
                });

               dialog = builder.create();
                dialog.getWindow().getDecorView().getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF303030));

                dialog.show();
                return true;
            case R.id.pref2:
                final CharSequence[] items1 = {"рандом","по порядку"};
                AlertDialog dialog1;
                // Creating and Building the Dialog
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);

                builder1.setTitle("Выберите порядок");
                builder1.setSingleChoiceItems(items1, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {


                        switch(item)
                        {
                            case 0:
                                preferences.saveData("order", "random");
                                break;
                            case 1:
                                preferences.saveData("order", "next");


                                break;


                        }
                        dialog.dismiss();
                    }
                });

                dialog1 = builder1.create();
                dialog1.getWindow().getDecorView().getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF303030));

                dialog1.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fr1 = new Fragment1_Gr();
        fr2 = new Fragment2_List();
        adapter.addFragment(fr1,"");
        adapter.addFragment(fr2,"");

        viewPager.setAdapter(adapter);


    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    void displayofDB(){
        fr1.displayDatabaseInfo();
        fr2.displayDatabaseInfo();
    }

   //В результате получения ссылки на фото
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK)    {

            SQLiteDatabase db = mDbHelper.getWritableDatabase(); //запись в базу

            ContentValues values = new ContentValues();
            switch (requestCode) {
                case ACTION_REQUEST_GALLERY:
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = "file://" + cursor.getString(columnIndex);
                    cursor.close();

                    values.put(PhotoContract.PhotoEntry.LINK_Photo, picturePath);

                    db.insert(PhotoContract.PhotoEntry.TABLE_NAME,null,values);
                    displayofDB();
                    break;

                case ACTION_REQUEST_CAMERA:

                    values.put(PhotoContract.PhotoEntry.LINK_Photo, initialURI+"");

                    db.insert(PhotoContract.PhotoEntry.TABLE_NAME,null,values);
                    displayofDB();
                    break;
            }

        }
    }




}
