package com.example.annar.testtask.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by annar on 30.09.2018.
 */

public class PhotoDbHelper extends SQLiteOpenHelper {

    private static final String DATABASENAME="listphoto.sql";
    private static final int DATABASEVER = 1;
    public PhotoDbHelper(Context context) {
        super(context,DATABASENAME,null,DATABASEVER);
    }

    @Override
    public void onCreate(SQLiteDatabase SQLiteDb) {

        //Строка базы данных
        String CREATE_TABLE_SQL = "CREATE TABLE " + PhotoContract.PhotoEntry.TABLE_NAME + " ("
                + PhotoContract.PhotoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "

                + PhotoContract.PhotoEntry.LINK_Photo + " TEXT NOT NULL);"                ;
        //Создадим базу данных
        SQLiteDb.execSQL(CREATE_TABLE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase SQLiteDb, int i, int i1) {
        //Пересоздание
        SQLiteDb.execSQL("DROP TABLE "+ PhotoContract.PhotoEntry.TABLE_NAME);
        onCreate(SQLiteDb);
    }
}
