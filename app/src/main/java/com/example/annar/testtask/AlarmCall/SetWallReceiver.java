package com.example.annar.testtask.AlarmCall;

import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.example.annar.testtask.DataBase.PhotoContract;
import com.example.annar.testtask.DataBase.PhotoDbHelper;
import com.example.annar.testtask.Photo.ItemPhoto;
import com.example.annar.testtask.Preference.Preferences;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by annar on 30.09.2018.
 */

public class SetWallReceiver  extends BroadcastReceiver {
    private ItemPhoto photo;
    private int position;
    private Preferences preferences;
    @Override
    public void onReceive(Context ctx, Intent intent) {
        preferences = new Preferences(ctx);
   //порядок и айди которая щас
        String order = preferences.getData("order");
        String id = preferences.getData("id");
        Log.i("info2", order);
        Log.i("info2", id);

        PhotoDbHelper mDbHelper = new PhotoDbHelper(ctx);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Зададим условие для выборки - список столбцов
        String[] projection = {
                PhotoContract.PhotoEntry._ID,
                PhotoContract.PhotoEntry.LINK_Photo
        };
        Cursor cursor = db.query(
                PhotoContract.PhotoEntry.TABLE_NAME,   // таблица
                projection,            // столбцы
                null,                  // столбцы для условия WHERE
                null,                  // значения для условия WHERE
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);                   // порядок сортировки
        try {


            if (order.equals("random")) {
                int size = (int) DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM ListPhoto", null);
                Random r = new Random();
                position = r.nextInt(size);
                Log.i("rand",position+"");
                cursor.moveToFirst();
                while (position != 0) {
                    cursor.moveToNext();
                    position--;
                }
                photo = new ItemPhoto(cursor.getString(0), cursor.getString(1));
                preferences.saveData("id", cursor.getString(0));

            } else {

                cursor.moveToFirst();
             if(cursor.getString(0).equals(id)) {

                 cursor.moveToNext();
                 photo = new ItemPhoto(cursor.getString(0), cursor.getString(1));

             }
                while(cursor.moveToNext()){
                    if(cursor.getString(0).equals(id)) {
                        if (cursor.moveToNext()) {
                            photo = new ItemPhoto(cursor.getString(0), cursor.getString(1));
                        } else {
                            cursor.moveToFirst();
                            photo = new ItemPhoto(cursor.getString(0), cursor.getString(1));
                        }
                        break;
                    }
                }

                preferences.saveData("id", cursor.getString(0));
            }


        } finally {
            // Всегда закрываем курсор после чтения
            cursor.close();
        }
        WallpaperManager myWallpaperManager
                = WallpaperManager.getInstance(ctx);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(ctx.getContentResolver(), Uri.parse(photo.getPhoto()));
            myWallpaperManager.setBitmap(bitmap);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
