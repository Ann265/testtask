package com.example.annar.testtask.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.annar.testtask.AlarmCall.SetWallReceiver;
import com.example.annar.testtask.Preference.Preferences;
import com.example.annar.testtask.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * Created by annar on 28.09.2018.
 */

public class Full_Image extends AppCompatActivity {

    private Button set;
    private ImageView fullImage;
    private String path, id;
    AlarmManager alarmMgr;;
    PendingIntent pendingIntent;
    private Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_image);
        Intent intent = getIntent();
        path = intent.getStringExtra("path");
        id = intent.getStringExtra("id");
        preferences = new Preferences(getApplicationContext());
        Log.i("info1",id);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final TextView titleBar = (TextView) findViewById(R.id.action_bar_title);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);

        titleBar.setText("Выбор картинки");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fullImage = (ImageView) findViewById(R.id.imageFull);
        set = (Button) findViewById(R.id.buttonSet);

        Picasso.with(this).load(path).into(fullImage);

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WallpaperManager myWallpaperManager
                        = WallpaperManager.getInstance(getApplicationContext());
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(path));
                    myWallpaperManager.setBitmap(bitmap);
                    preferences.saveData("id", id);

                    stopAlarmManager();
                    startAlarmManager();

                    finish();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

}
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public void stopAlarmManager()
    {
        if(alarmMgr != null)
            alarmMgr.cancel(pendingIntent);
    }
    public void startAlarmManager()
    {
        int minute = 30; // 30 минут если ничего не выбрали

        if (preferences.getData("minute").equals("") == false) {
             minute = Integer.valueOf(preferences.getData("minute"));
        }


        Intent dialogIntent = new Intent(getBaseContext(), SetWallReceiver.class);



        alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        pendingIntent = PendingIntent.getBroadcast(this, 0, dialogIntent, 0);


        alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime()+minute*60*1000,  minute*60 * 1000, pendingIntent);

    }

}