package com.example.annar.testtask.DataBase;

import android.provider.BaseColumns;

/**
 * Created by annar on 30.09.2018.
 */

public final class PhotoContract {
    private PhotoContract() {
    };

    public static final class PhotoEntry implements BaseColumns {
        public final static String TABLE_NAME = "ListPhoto";
        public final static String _ID = BaseColumns._ID;

        public final static String LINK_Photo = "link";

    }
}
