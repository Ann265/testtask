package com.example.annar.testtask.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.annar.testtask.Activity.Full_Image;
import com.example.annar.testtask.Photo.ItemPhoto;
import com.example.annar.testtask.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by annar on 24.09.2018.
 */

public class RecyclerViewAdapter extends  RecyclerView.Adapter<RecyclerViewHolders>{

    private static ArrayList<String> listDelete;
    private List<ItemPhoto> itemList;
        private Context context;
        private Activity activity;
        private String task, role, place;
        AlertDialog.Builder builder;



        public RecyclerViewAdapter(List<ItemPhoto> itemList, Context context, Activity activity) {
            this.itemList = itemList;
            this.context = context;
            this.activity = activity;
            listDelete=new ArrayList<String>();
        }



        @Override
        public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_pict, null);
            RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {
           // holder.photo.setImageDrawable(context.getDrawable(itemList.get(position).getPhoto()));
            holder.gal.setVisibility(View.INVISIBLE);
            Picasso.with(context)
                    .load(itemList.get(position).getPhoto()).into(holder.photo);
            holder.itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent theIntent = new Intent(activity, Full_Image.class);
                    theIntent.putExtra("path", itemList.get(position).getPhoto());
                    theIntent.putExtra("id", itemList.get(position).getId());

                    activity.startActivity(theIntent);

                }
            });
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (holder.gal.getVisibility() == View.VISIBLE){
                        holder.gal.setVisibility(View.INVISIBLE);
                        listDelete.remove( itemList.get(position).getId());
                    } else {
                        holder.gal.setVisibility(View.VISIBLE);
                        listDelete.add( itemList.get(position).getId());
                    }
                    return true;
                }
            });



        }
        public static ArrayList <String> getListDelete () {
            return listDelete;
        }
    public static void clearList( ) {

        Iterator<String> i = listDelete.iterator();
        while (i.hasNext()) {
            String s = i.next();

            i.remove();
        }
    }


        @Override
        public int getItemCount() {
            return this.itemList.size();
        }
}
