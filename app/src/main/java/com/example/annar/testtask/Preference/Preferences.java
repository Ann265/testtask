package com.example.annar.testtask.Preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by annar on 30.09.2018.
 */

public class Preferences {

        private static Preferences yourPreference;
        private SharedPreferences preferences ;


        public Preferences(Context context) {
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }

        public void saveData(String key,String value) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(key, value);
            editor.apply();
        }

        public String getData(String key) {
            if (preferences!= null) {
                return preferences.getString(key, "");
            }
            return "";
        }

}
